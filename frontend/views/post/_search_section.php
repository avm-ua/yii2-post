<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use ict\posts\backend\components\SectionHelper;
use dosamigos\selectize\SelectizeTextInput;
use akavov\countries\widgets\CountriesSelectizeTextInput;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model ict\posts\frontend\models\PostSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-search">

    <?php $form = ActiveForm::begin([
        'action' => ['section', 'slug' => $section['slug']],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <?= $form->field($model, 'title', ['options' => ['class' => 'col-sm-3']]); ?>

        <?= $form->field($model, 'published[0]', ['options' => ['class' => 'col-sm-3']])->widget(DateTimePicker::className(),[
            'options' => ['placeholder' => Yii::t('post', 'Select date from')],
            'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
            'removeButton' => false,
            'size' => 'md',
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii',
                'todayHighlight' => true,
                'todayBtn' => true,
                'autoclose' => true,
            ]
        ]); ?>

        <?= $form->field($model, 'published[1]', ['options' => ['class' => 'col-sm-3']])->widget(DateTimePicker::className(),[
            'options' => ['placeholder' => Yii::t('post', 'Select date to')],
            'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
            'removeButton' => false,
            'size' => 'md',
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii',
                'todayHighlight' => true,
                'todayBtn' => true,
                'autoclose' => true,
            ]
        ]); ?>

        <?= $form->field($model, 'countryValues', ['options' => ['class' => 'col-sm-3']])->widget(CountriesSelectizeTextInput::className(), [
                'customRender' => [
                    'item'  => '<div> <span class="label flag flag-icon-background flag-icon-{item.alpha}">&nbsp;</span>&nbsp;<span class="name">{escape(item.name_en)}</span></div>',
                    'option'  => '<div> <span class="label flag flag-icon-background flag-icon-{item.alpha}">&nbsp;</span>&nbsp;<span class="name">{escape(item.name_en)}</span></div>',
                ],
                'clientOptions' => [
                    'valueField' => 'name_en',
                    'labelField' => 'name_en',
                    'searchField' => ['name_en', 'name_uk', 'name_ru'],
                    'plugins' => ['remove_button'],
                    'closeAfterSelect' => true,
                    'maxItems' => 10,
                    'delimiter' => ',',
                    'persist' => false,
                    'preload' => true,
                    'items' => $model->countryValues,
                    'create' => false,
                ],
            ]
        ); ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('post', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('post', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
