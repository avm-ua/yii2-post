<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel ict\posts\frontend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $section['name'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_search_section', ['model' => $searchModel, 'section' => $section]); ?>
    <?php /*var_dump($dataProvider); */?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'layout' => '{items}{pager}',
        'itemView' => '_list_section',
    ]) ?>

</div>
