<?php

namespace ict\posts\frontend\controllers;


use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use ict\posts\Module;
use ict\posts\common\models\Post;
use ict\posts\frontend\models\PostSearch;
use ict\posts\common\models\Section;

/**
 * Frontend PostController.
 */
class PostController extends Controller
{
    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists Post models by section.
     * @param string $slug section slug id
     * @return mixed
     */
    public function actionSection($slug)
    {
        $section = Section::find()
            ->where([
                'slug' => urlencode(Html::encode($slug)),
                'lang' => $this->module->getAppLanguageCode(),
            ])
            ->asArray()
            ->one();

        $searchModel = new PostSearch();

        $dataProvider = $searchModel->searchSection($section, Yii::$app->request->queryParams);

        return $this->render('section', [
            'section' => $section,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds and displays a single Post model based on its slug value.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionShow($slug)
    {
        $model = Post::find()->where([
            'slug' => urlencode(Html::encode($slug)),
            'lang' => $this->module->getAppLanguageCode(),
        ])->asArray()->one();

        if ($model !== null) {
            // Attention passing model array itself not ['model' => $model]
            return $this->render('show', $model);
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'The requested page does not exist.'));
        }
    }
}
