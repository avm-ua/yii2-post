<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use mihaildev\elfinder\ElFinder;
use dosamigos\ckeditor\CKEditor;
use dosamigos\selectize\SelectizeTextInput;
use ict\posts\Module;
use ict\posts\common\components\ElFinderInput;
use ict\posts\backend\components\SectionHelper;
use akavov\countries\widgets\CountriesSelectizeTextInput;

/* @var $this yii\web\View */
/* @var $model ict\posts\common\models\Post */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('$("#post-title").keyup(function(e){
    $.get("'.Url::to(["/{$this->context->module->id}/post/slug"]).'",{
        str:$("#post-title").val()
    }).done(function(slug){
        $("#post-slug").val(slug);
    });
});', $this::POS_END, 'slug');

?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'title', ['options' =>['class' => 'col-sm-6']])->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'slug', ['options' =>['class' => 'col-sm-6']])->textInput(['maxlength' => true]); ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'published', ['options' =>['class' => 'col-sm-6']])->widget(DateTimePicker::className(),[
            'options' => ['placeholder' => Yii::t('post', 'Select post publication date and time')],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii',
                'autoclose' => true,
                'calendarWeeks' => true,
                'todayHighlight' => true,
                'todayBtn' => true,
                'weekStart' => 1,
            ]
        ]); ?>

        <?= $form->field($model, 'sections', ['options' =>['class' => 'col-sm-6']])->widget(SelectizeTextInput::className(), [
            'clientOptions' => [
                'options' => SectionHelper::getSelectizeSectionsList($model->lang),
                'labelField' => 'name',
                'valueField' => 'id',
                'searchField' => 'name',
                'preload' => true,
                'maxItems' => 5,
                'plugins' => ['remove_button'],
                'inputClass' => 'form-control selectize-input',
            ],
        ]); ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'lead', ['options' =>['class' => 'col-sm-3']])->widget(CKEditor::className(), [
            'clientOptions' => [
                'forcePasteAsPlainText' => true,
                'language' => Module::getAppLanguageCode(),
                'height' => '350',
                'toolbar' => [
                    ['name' => 'row1', 'items' => [
                        'Source', '-',
                        'Bold', 'Italic', 'Underline', 'Strike', '-',
                    ]],
                    ['name' => 'row2', 'items' => [
                        'Subscript', 'Superscript', 'RemoveFormat', '-',
                        'TextColor', '-',
                        'Link', 'Unlink', 'Anchor', '-',
                    ]]
                ],
            ]
        ]); ?>

        <?= $form->field($model, 'content', ['options' =>['class' => 'col-sm-9']])->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'custom',
            'clientOptions' => ArrayHelper::merge(
                ElFinder::ckeditorOptions('post/elfinder'), [
                'forcePasteAsPlainText' => true,
                'height' => '350',
                'language' => Module::getAppLanguageCode(),
                'toolbar' => [
                    ['name' => 'row1', 'items' => [
                        'Source', '-',
                        'Bold', 'Italic', 'Underline', 'Strike', '-',
                        'Subscript', 'Superscript', 'RemoveFormat', '-',
                        'TextColor', 'BGColor', '-',
                        'NumberedList', 'BulletedList', '-',
                        'Outdent', 'Indent', '-', 'Blockquote', '-',
                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'list', 'indent', 'blocks', 'align', 'bidi', '-',
                        'Link', 'Unlink', 'Anchor', '-',
                        'ShowBlocks', 'Maximize',
                    ]],
                    ['name' => 'row2', 'items' => [
                        'Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe', '-',
                        'NewPage', 'Print', 'Templates', '-',
                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-',
                        'Undo', 'Redo', '-',
                        'Find', 'SelectAll', 'Format', 'FontSize',
                    ]],
                ]
            ])
        ]); ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'img', ['options' => ['class' => 'col-sm-4']])->widget(ElFinderInput::className()); ?>

        <?= $form->field($model, 'img_desc', ['options' => ['class' => 'col-sm-4']])->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'img_src', ['options' => ['class' => 'col-sm-4']])->textInput(['maxlength' => true]); ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'tagValues', ['options' =>['class' => 'col-sm-4']])->widget(SelectizeTextInput::className(), [
            'loadUrl' => ['/post/post/search-tags'],
            'options' => ['disabled' => !$this->context->module->useTags],
            'clientOptions' => [
                'valueField' => 'name',
                'labelField' => 'name',
                'searchField' => ['name'],
                'plugins' => ['remove_button'],
                'maxItems' => 10,
                'delimiter' => ',',
                'persist' => false,
                'createOnBlur' => true,
                'create' => true
            ],
        ]); ?>

        <?= $form->field($model, 'authorValues', ['options' =>['class' => 'col-sm-4']])->widget(SelectizeTextInput::className(), [
            'loadUrl' => ['/post/post/search-authors'],
            'options' => ['disabled' => !$this->context->module->useAuthors],
            'clientOptions' => [
                'valueField' => 'name',
                'labelField' => 'name',
                'searchField' => ['name'],
                'plugins' => ['remove_button'],
                'maxItems' => 1,
                'delimiter' => ',',
                'persist' => false,
                'createOnBlur' => true,
                'create' => true
            ],
        ]); ?>

        <?= $form->field($model, 'countryValues', ['options' =>['class' => 'col-sm-4']])->widget(CountriesSelectizeTextInput::className(), [
            'customRender' => [
                'item'  => '<div> <span class="label flag flag-icon-background flag-icon-{item.alpha}">&nbsp;</span>&nbsp;<span class="name">{escape(item.name_en)}</span></div>',
                'option'  => '<div> <span class="label flag flag-icon-background flag-icon-{item.alpha}">&nbsp;</span>&nbsp;<span class="name">{escape(item.name_en)}</span></div>',
            ],
            'options' => ['disabled' => !$this->context->module->useCountries],
            'clientOptions' => [
                'valueField' => 'name_en',
                'labelField' => 'name_en',
                'searchField' => ['name_en', 'name_uk', 'name_ru'],
                'plugins' => ['remove_button'],
                'closeAfterSelect' => true,
                'maxItems' => 10,
                'delimiter' => ',',
                'persist' => false,
                'preload' => true,
                'items' => $model->countryValues,
                'create' => false,
            ]
        ]); ?>
    </div>

    <div class="row">
        <?= $form->field($model, 'draft', ['options' => ['class' => 'col-sm-4']])->dropDownList([Yii::t('yii', 'No'), Yii::t('yii', 'Yes')]); ?>

        <?= $form->field($model, 'status', ['options' => ['class' => 'col-sm-4']])->dropDownList($this->context->module->postStatuses); ?>

        <?= $form->field($model, 'enable_comments', ['options' => ['class' => 'col-sm-4']])->dropDownList(
            [Yii::t('yii', 'No'), Yii::t('yii', 'Yes')],
            ['disabled' => !$this->context->module->enableComments]
        ); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('yii', 'Update'), [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
