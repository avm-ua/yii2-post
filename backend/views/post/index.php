<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use ict\posts\common\components\BaseHelper;

/* @var $this         yii\web\View
 * @var $searchModel  ict\posts\common\models\PostSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $langForm     yii\base\DynamicModel
 */

$this->title = Yii::t('post', 'Posts');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => 'col-sm-4']]); ?>

            <?= $form->field($langForm, 'lang', [
                'inputTemplate' => '<div class="input-group"><div class="input-group-btn">'.
                    Html::submitButton(Yii::t('post', 'Create post'), ['class' => 'btn btn-success']).'
                </div>{input}</div>',
            ])->dropDownList($this->context->module->languages)->label(false); ?>

        <?php ActiveForm::end(); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'lang',
                'filter' => $this->context->module->languages,
                'value' => function($data) {
                    return $this->context->module->languages[$data->lang];
                },
            ],
            'created',
            'updated',
            'published',
            'title',
            [
                'attribute' => 'draft',
                'format' => 'html',
                'filter' => [
                    0 => Yii::t('yii', 'No'),
                    1 => Yii::t('yii', 'Yes'),
                ],
                'value' => function($data) {
                    if ((bool)$data->draft) {
                        return Html::tag('span', Yii::t('yii', 'Yes'), ['class' => 'text-danger']);
                    } elseif (!(bool)$data->draft && BaseHelper::checkIsPublished($data->published)) {
                        return Html::tag('span', Yii::t('yii', 'No'), ['class' => 'text-success']);
                    } else {
                        return Html::tag('span', Yii::t('yii', Yii::t('post', 'On timer')), ['class' => 'text-warning']);
                    }
                },
            ],
            [
                'attribute' => 'status',
                'filter' => $this->context->module->postStatuses,
                'value' => function ($data) {
                    return $this->context->module->postStatuses[$data->status];
                },
            ],
            'view_count',
            // 'slug',
            // 'lead:ntext',
            // 'content:ntext',
            // 'img',
            // 'img_desc',
            // 'img_src',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
