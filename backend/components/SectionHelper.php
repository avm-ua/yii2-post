<?php

namespace ict\posts\backend\components;

use Yii;
use yii\base\Object;
use yii\helpers\ArrayHelper;
use ict\posts\common\models\Section;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Json;

/**
 * Basic Helper component used all over the module
 *
 * @category Helpers
 * @package  Posts
 * @author   Bogdan Fedun <delagics@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     http://delagics.com
 */
class SectionHelper extends Object
{
    /**
     * Gets available sections
     * @param string $lang sections language
     * @param null|integer $excludedId Pass an ID of a sections that must be excluded from select
     * @param bool|true    $asArray    Return as array
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSectionsList($lang, $excludedId = null, $asArray = true)
    {
        $query = Section::find()
            ->select(['id', 'lang', 'name'])
            ->where(['lang' => Html::encode($lang)]);
        // Remove current section from sections list
        ($excludedId) ? $query->andWhere(['!=', 'id', $excludedId]) : false;
        // Return data as array
        ($asArray) ? $query->asArray() : false;
        return $query->all();
    }

    /**
     * Get available sections that is ready for usage with Selectize plugin
     * @param string $lang sections language
     * @return JsExpression
     */
    public function getSelectizeSectionsList($lang)
    {
        $output = [];
        $sections = ArrayHelper::map(self::getSectionsList($lang), 'id', 'name');
        if ($sections) {
            foreach ($sections as $id => $name) {
                $output[] = ['id' => $id, 'name' => $name];
            }
        }
        return new JsExpression(Json::encode($output));
    }
}