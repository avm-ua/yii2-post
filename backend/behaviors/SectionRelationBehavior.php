<?php

namespace ict\posts\backend\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use ict\posts\common\models\PostSectionRelation;

/**
 * SectionRelationBehavior provides convenient way of establishing relations from current class
 * to relative one, via junction table.
 * @package posts
 */
class SectionRelationBehavior extends Behavior
{
    /**
     * @var string $attribute owner attribute to which behavior is applied
     */
    public $attribute;

    /**
     * @var string $ownerRelationAttribute relation attribute of the current class
     */
    public $ownerRelationAttribute = 'post_id';

    /**
     * @var string $sectionRelationAttribute relation attribute of the related class Section
     */
    public $sectionRelationAttribute = 'section_id';

    /**
     * @var string $relationName owner relation name
     */
    public $relationName;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_UPDATE => 'proceedRelations',
            ActiveRecord::EVENT_AFTER_INSERT => 'proceedRelations',
        ];
    }

    /**
     * Preparing attribute value for selectize plugin
     * @return void
     */
    public function afterFind()
    {
        $foundRelations = $this->owner->getRelation($this->relationName)
            ->select($this->sectionRelationAttribute)
            ->asArray()
            ->all();

        if ($foundRelations) {
            $this->owner->{$this->attribute} = implode(',', ArrayHelper::getColumn($foundRelations, $this->sectionRelationAttribute));
        }
    }

    /**
     * Deals with saving, updating and deleting relations
     * @return void
     */
    public function proceedRelations()
    {
        $chosenSections = ($this->owner->{$this->attribute}) ? explode(',', $this->owner->{$this->attribute}) : [];
        // If we update existing record, perform check and update old relations
        if (!$this->owner->isNewRecord) {
            $oldRelations = PostSectionRelation::find()
                ->where([$this->ownerRelationAttribute => $this->owner->primaryKey])
                ->all();
            foreach ($chosenSections as $x => $chosenSectionId) {
                foreach ($oldRelations as $i => $oldRelation) {
                    // Check if there are relations that should not be removed
                    if ($oldRelation->{$this->sectionRelationAttribute} == $chosenSectionId) {
                        unset($oldRelations[$i]);
                        unset($chosenSections[$x]);
                    }
                }
            }
            // Remove old relations that have not been selected this time
            if ($oldRelations) {
                $this->removeOldRelations($oldRelations);
            }
            // Insert new relations that have been added this time
            if ($chosenSections) {
                $this->insertNewRelations($chosenSections);
            }
        } else {
            // Insert totally new relations, cause we have a new owner record
            if ($chosenSections) {
                $this->insertNewRelations($chosenSections);
            }
        }
    }

    /**
     * Remove old relations connected with `$this->owner`
     * @param PostSectionRelation[] $oldRelations
     * @return void
     */
    public function removeOldRelations($oldRelations)
    {
        foreach ($oldRelations as $oldRelation) {
            $this->owner->unlink($this->relationName, $oldRelation, true);
        }
    }

    /**
     * Insert new relations
     * @param array $newSections
     * @return void
     */
    public function insertNewRelations($newSections)
    {
        if ($newSections && is_array($newSections)) {
            foreach ($newSections as $newSectionId) {
                $model = new PostSectionRelation();
                $model->{$this->ownerRelationAttribute} = $this->owner->primaryKey;
                $model->{$this->sectionRelationAttribute} = $newSectionId;
                if ($model->validate()) {
                    $this->owner->link($this->relationName, $model);
                }
            }
        }
    }
}
