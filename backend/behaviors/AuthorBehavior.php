<?php
/**
 * Created by PhpStorm.
 * User: turko_v
 * Date: 05.10.2015
 * Time: 15:48
 */

namespace ict\posts\backend\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;

class AuthorBehavior extends Behavior
{
    /**
     * @var boolean whether to return authors as array instead of string
     */
    public $authorValuesAsArray = false;
    /**
     * @var string the authors relation name
     */
    public $authorRelation = 'authors';
    /**
     * @var string the authors model value attribute name
     */
    public $authorValueAttribute = 'name';
    /**
     * @var string|false the authors model frequency attribute name
     */
    public $authorFrequencyAttribute = 'frequency';
    /**
     * @var string[]
     */
    private $_authorValues;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * Returns authors.
     * @param boolean|null $asArray
     * @return string|string[]
     */
    public function getAuthorValues($asArray = null)
    {
        if (!$this->owner->getIsNewRecord() && $this->_authorValues === null) {
            $this->_authorValues = [];

            /* @var ActiveRecord $author */
            foreach ($this->owner->{$this->authorRelation} as $author) {
                $this->_authorValues[] = $author->getAttribute($this->authorValueAttribute);
            }
        }

        if ($asArray === null) {
            $asArray = $this->authorValuesAsArray;
        }

        if ($asArray) {
            return $this->_authorValues === null ? [] : $this->_authorValues;
        } else {
            return $this->_authorValues === null ? '' : implode(', ', $this->_authorValues);
        }
    }

    /**
     * Sets authors.
     * @param string|string[] $values
     */
    public function setAuthorValues($values)
    {
        $this->_authorValues = $this->filterAuthorValues($values);
    }

    /**
     * Adds authors.
     * @param string|string[] $values
     */
    public function addAuthorValues($values)
    {
        $this->_authorValues = array_unique(array_merge($this->getAuthorValues(true), $this->filterAuthorValues($values)));
    }

    /**
     * Removes authors.
     * @param string|string[] $values
     */
    public function removeAuthorValues($values)
    {
        $this->_authorValues = array_diff($this->getAuthorValues(true), $this->filterAuthorValues($values));
    }

    /**
     * Removes all authors.
     */
    public function removeAllAuthorValues()
    {
        $this->_authorValues = [];
    }

    /**
     * Returns a value indicating whether authors exists.
     * @param string|string[] $values
     * @return boolean
     */
    public function hasAuthorValues($values)
    {
        $authorValues = $this->getAuthorValues(true);

        foreach ($this->filterAuthorValues($values) as $value) {
            if (!in_array($value, $authorValues)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return void
     */
    public function afterSave()
    {
        if ($this->_authorValues === null) {
            return;
        }

        if (!$this->owner->getIsNewRecord()) {
            $this->beforeDelete();
        }

        $authorRelation = $this->owner->getRelation($this->authorRelation);
        $pivot = $authorRelation->via->from[0];
        /* @var ActiveRecord $class */
        $class = $authorRelation->modelClass;
        $rows = [];

        foreach ($this->_authorValues as $value) {
            /* @var ActiveRecord $author */
            $author = $class::findOne([$this->authorValueAttribute => $value]);

            if ($author === null) {
                $author = new $class();
                $author->setAttribute($this->authorValueAttribute, $value);
            }

            if ($this->authorFrequencyAttribute !== false) {
                $frequency = $author->getAttribute($this->authorFrequencyAttribute);
                $author->setAttribute($this->authorFrequencyAttribute, ++$frequency);
            }

            if ($author->save()) {
                $rows[] = [$this->owner->getPrimaryKey(), $author->getPrimaryKey()];
            }
        }

        if (!empty($rows)) {
            $this->owner->getDb()
                ->createCommand()
                ->batchInsert($pivot, [key($authorRelation->via->link), current($authorRelation->link)], $rows)
                ->execute();
        }
    }

    /**
     * @return void
     */
    public function beforeDelete()
    {
        $authorRelation = $this->owner->getRelation($this->authorRelation);
        $pivot = $authorRelation->via->from[0];

        if ($this->authorFrequencyAttribute !== false) {
            /* @var ActiveRecord $class */
            $class = $authorRelation->modelClass;

            $pks = (new Query())
                ->select(current($authorRelation->link))
                ->from($pivot)
                ->where([key($authorRelation->via->link) => $this->owner->getPrimaryKey()])
                ->column($this->owner->getDb());

            if (!empty($pks)) {
                $class::updateAllCounters([$this->authorFrequencyAttribute => -1], ['in', $class::primaryKey(), $pks]);
            }
        }

        $this->owner->getDb()
            ->createCommand()
            ->delete($pivot, [key($authorRelation->via->link) => $this->owner->getPrimaryKey()])
            ->execute();
    }

    /**
     * Filters authors.
     * @param string|string[] $values
     * @return string[]
     */
    public function filterAuthorValues($values)
    {
        return array_unique(preg_split(
            '/\s*,\s*/u',
            preg_replace('/\s+/u', ' ', is_array($values) ? implode(',', $values) : $values),
            -1,
            PREG_SPLIT_NO_EMPTY
        ));
    }
}