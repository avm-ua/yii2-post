<?php

namespace ict\posts\backend\controllers;

use Yii;
use mihaildev\elfinder\PathController;

class ElfinderController extends PathController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->access = ['@'];
        $this->root = [
            //Specify 'baseUrl', 'basePath' without trailing slashes
            'baseUrl' => $this->module->imgPaths['url'],
            'basePath' => $this->module->imgPaths['path'],
        ];
    }
}
