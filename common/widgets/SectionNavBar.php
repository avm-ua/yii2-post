<?php

namespace ict\posts\common\widgets;

use Yii;
use yii\base\InvalidParamException;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Widget;
use ict\posts\common\models\Section;
use yii\helpers\Url;

class SectionNavBar extends Widget
{
    /**
     * @var integer switch constants used for `site_disabled`, `nav_disabled` conditions.
     */
    const ENABLED = 1;
    const DISABLED = 0;

    /**
     * @var string default item base URL. Used to create links for items in [[static::parseUrl()]].
     */
    public $defaultBaseUrl = '/post/post/section';

    /**
     * @var string default item URL parameter name. Used to create links for items in [[static::parseUrl()]].
     */
    public $defaultUrlParamName = 'slug';

    /**
     * @var array static widget config. Filled in [[init()]].
     */
    public static $cfg =[];

    /**
     * @var array [[yii\bootstrap\NavBar]] options.
     */
    public $navBarOptions = [
        'brandLabel' => 'My Company',
        'brandUrl' => '/',
        'options' => ['class' => 'navbar-inverse navbar-fixed-top'],
    ];

    /**
     * @var array [[yii\bootstrap\Nav]] widget options.
     */
    public $navOptions = [
        'options' => ['class' => 'navbar-nav'],
    ];

    /**
     * @var array of additional nav links that comes before sections nav links.
     */
    public $beforeNavLinks = [];

    /**
     * @var array of additional nav links that comes after sections nav links.
     */
    public $afterNavLinks = [];

    /**
     * Initialize widget parameters
     */
    public function init()
    {
        parent::init();

        static::$cfg = [
            'defaultBaseUrl' => $this->defaultBaseUrl,
            'defaultUrlParamName' => $this->defaultUrlParamName,
            'beforeNavLinks' => $this->beforeNavLinks,
            'afterNavLinks' => $this->afterNavLinks,
        ];
    }

    /**
     * Run widget
     */
    public function run()
    {
        static::renderWidget($this->navBarOptions, $this->navOptions);
    }

    /**
     * Render widget.
     * @param array $navBarOptions
     * @param array $navOptions
     * @uses static::fetchItems()
     * @uses static::prepareTree()
     */
    public static function renderWidget($navBarOptions, $navOptions)
    {
        if (!isset($navOptions['items'])) {
            $items = static::fetchItems('uk');
            $output = static::prepareTree($items, 0);
            //TODO: add ability to specify order for additional nav links
            if (static::$cfg['beforeNavLinks'] || static::$cfg['afterNavLinks']) {
                $output = array_merge(static::$cfg['beforeNavLinks'], $output, static::$cfg['afterNavLinks']);
            }
            $navOptions['items'] = $output;
        }
        NavBar::begin($navBarOptions);
        echo Nav::widget($navOptions);
        NavBar::end();
    }

    /**
     * Gets available sections from DB.
     * @param string $lang sections language
     * @return array|\yii\db\ActiveRecord[]
     * @throws InvalidParamException when $lang parameter not well configured or can't be assigned
     */
    public static function fetchItems($lang = null)
    {
        if ($lang === null) {
            if (strlen(Yii::$app->language) === 5) {
                $lang = explode('-', Yii::$app->language)[0];
            } elseif (strlen(Yii::$app->language) === 2) {
                $lang = Yii::$app->language;
            } else {
                throw new InvalidParamException(Yii::t(
                    'event',
                    'Can\'t assign a value to a $lang parameter, application language seems to be not well configured.'
                ));
            }
        }
        if (!is_string($lang) || strlen($lang) < 2 || strlen($lang) > 5) {
            throw new InvalidParamException('Parameter $lang not well configured.');
        }
        return Section::find()
            ->select(['id', 'name', 'slug', 'parent_id', 'nav_sort'])
            ->where(['lang' => $lang])
            ->andWhere(['site_disabled' => static::DISABLED])
            ->andWhere(['nav_disabled' => static::DISABLED])
            ->orderBy(['nav_sort' => SORT_DESC])
            ->indexBy('id')
            ->asArray()
            ->all();
    }

    /**
     * Prepare sections tree as an array.
     * @param array $items
     * @param integer $parent
     * @return array
     */
    public static function prepareTree($items, $parent = 0){
        $output = [];
        foreach($items as $i => $item) {
            if($item['parent_id'] == $parent) {
                $output[$item['id']] = [
                    'label' => $item['name'],
                    'url' => static::parseUrl($item['slug']),
                ];
                // Checking for children items
                if ($children = static::prepareTree($items, $item['id'])) {
                    $output[$i]['items'] = $children;
                }
            }
        }
        return $output;
    }

    /**
     * Prepare item URL for [[yii\helpers\Url::to()]].
     * @param string $url
     * @return array|string
     */
    public static function parseUrl($url)
    {
        $url = trim($url, '/');
        if (preg_match('/^.*\?.*\=.*/', $url)) {
            $urlParamName  = substr($url, strpos($url, '?') +1, - strpos($url, '=') -1);
            $urlParamValue = substr($url, strpos($url, '=') +1, strlen($url) -1);
            $baseUrl = '/' . substr($url, 0, strpos($url, '?'));
            return Url::to([$baseUrl, $urlParamName => $urlParamValue]);
        } elseif (preg_match('/\//', $url)) {
            $baseUrl = '/' . $url;
            return [$baseUrl];
        } else {
            return Url::to([static::$cfg['defaultBaseUrl'], static::$cfg['defaultUrlParamName'] => $url]);
        }
    }
}
