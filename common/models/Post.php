<?php

namespace ict\posts\common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use ict\posts\Module;
use ict\posts\backend\behaviors\SectionRelationBehavior;
use ict\posts\backend\behaviors\AuthorBehavior;
use creocoder\taggable\TaggableBehavior;
use akavov\countries\components\CountriesBehavior;


/**
 * This is the model class for table "{{%post}}".
 *
 * @property integer $id
 * @property string $lang
 * @property string $created
 * @property string $updated
 * @property string $published
 * @property string $title
 * @property string $slug
 * @property string $lead
 * @property string $content
 * @property string $img
 * @property string $img_desc
 * @property string $img_src
 * @property integer $draft
 * @property integer $enable_comments
 * @property integer $view_count
 * @property integer $status
 *
 * @property PostSectionRelation[] $postSectionRelations
 */
class Post extends ActiveRecord
{
    /**
     * @var string
     */
    public $sections;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'updated',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SectionRelationBehavior::className(),
                'attribute' => 'sections',
                'relationName' => 'postSectionRelations',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init(){
        parent::init();
        $module = Module::getInstance();
        // activate Taggable behavior
        if($module->useTags){
            $this->attachBehavior('taggable', ['class'=>TaggableBehavior::className()]);
        }
        // activate author behavior
        if($module->useAuthors){
            $this->attachBehavior('author', ['class'=>AuthorBehavior::className()]);
        }
        // activate countries behavior
        if($module->useCountries){
            $this->attachBehavior('countries', ['class'=>CountriesBehavior::className()]);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'published', 'title', 'slug', 'content', 'draft', 'status'], 'required'],
            [['created', 'updated', 'published', 'sections', 'enable_comments', 'lead', 'img', 'img_desc', 'img_src'], 'safe'],
            [['lead', 'content'], 'string'],
            [['draft', 'enable_comments', 'view_count', 'status'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title', 'slug', 'img', 'img_desc', 'img_src'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            ['tagValues', 'safe'], // use for taggable behavior
            ['authorValues', 'safe'], // use for author behavior
            ['countryValues', 'safe'] // use for countries behavior
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('post', 'ID'),
            'lang' => Yii::t('post', 'Language'),
            'created' => Yii::t('post', 'Created'),
            'updated' => Yii::t('post', 'Updated'),
            'published' => Yii::t('post', 'Published'),
            'title' => Yii::t('post', 'Title'),
            'slug' => Yii::t('post', 'Slug'),
            'lead' => Yii::t('post', 'Lead'),
            'content' => Yii::t('post', 'Content'),
            'img' => Yii::t('post', 'Image'),
            'img_desc' => Yii::t('post', 'Image description'),
            'img_src' => Yii::t('post', 'Image source'),
            'draft' => Yii::t('post', 'Draft'),
            'enable_comments' => Yii::t('post', 'Enable comments'),
            'view_count' => Yii::t('post', 'View count'),
            'status' => Yii::t('post', 'Status'),
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('{{%post_tag_assn}}', ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Author::className(), ['id' => 'author_id'])->viaTable('{{%post_author_assn}}', ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostSectionRelations()
    {
        return $this->hasMany(PostSectionRelation::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Section::className(), ['id' => 'section_id'])->via('postSectionRelations');
    }

    /**
     * increment value view_count
     */
    public function viewCountInc(){
        $this->updateCounters(['view_count' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])
            ->viaTable('{{%post_country_assn}}', ['post_id' => 'id']);
    }
}
