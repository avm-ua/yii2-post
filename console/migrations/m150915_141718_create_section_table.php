<?php

use yii\db\Migration;

class m150915_141718_create_section_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        echo "Creating tables.\n";
        $this->createTable('{{%section}}', [
            'id'            => $this->primaryKey(),
            'lang'          => $this->string(5)->notNull(),
            'name'          => $this->string(255)->notNull(),
            'slug'          => $this->string(255)->unique()->notNull(),
            'description'   => $this->text()->notNull(),
            'parent_id'     => $this->integer(11)->notNull(),
            'site_disabled' => $this->integer(1)->notNull(),
            'nav_disabled'  => $this->integer(1)->notNull(),
            'nav_sort'      => $this->integer(11)->notNull(),
        ], $tableOptions);

        // Index for faster sections SELECT by slug
        $this->createIndex('slug_ix', '{{%section}}', 'slug', true);
    }

    public function safeDown()
    {
        echo "Removing tables.\n";
        $this->dropTable('{{%section}}');
    }

}
