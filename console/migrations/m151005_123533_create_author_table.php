<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_123533_create_author_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%author}}', [
            'id'        => $this->primaryKey(11),
            'name'      => $this->string(255)->notNull(),
            'frequency' => $this->integer(11)->notNull()->defaultValue(0),
        ]);

    }

    public function safeDown()
    {
        echo "Removing tables.\n";
        $this->dropTable('{{%author}}');
    }

}
