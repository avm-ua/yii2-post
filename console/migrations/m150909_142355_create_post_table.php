<?php

use yii\db\Migration;

class m150909_142355_create_post_table extends Migration
{
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post}}', [
            'id'              => $this->primaryKey(),
            'lang'            => $this->string(5)->notNull(),
            'created'         => $this->dateTime()->notNull(),
            'updated'         => $this->dateTime()->notNull(),
            'published'       => $this->dateTime()->notNull(),
            'title'           => $this->string(255)->notNull(),
            'slug'            => $this->string(255)->unique()->notNull(),
            'lead'            => $this->text()->notNull(),
            'content'         => $this->text()->notNull(),
            'img'             => $this->string(255)->notNull(),
            'img_desc'        => $this->string(255)->notNull(),
            'img_src'         => $this->string(255)->notNull(),
            'draft'           => $this->integer(1)->notNull(),
            'enable_comments' => $this->integer(1)->defaultValue(1)->notNull(),
            'view_count'      => $this->integer(11)->notNull(),
            'status'          => $this->integer(2)->notNull(),
        ], $tableOptions);

        // Index for faster sections SELECT by slug
        $this->createIndex('slug_ix', '{{%post}}', 'slug', true);
    }

    public function safeDown()
    {

        echo "Removing tables.\n";
        $this->dropTable('{{%post}}');
    }
}
