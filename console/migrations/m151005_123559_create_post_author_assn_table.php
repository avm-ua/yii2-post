<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_123559_create_post_author_assn_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%post_author_assn}}', [
            'post_id'   => $this->integer(11)->notNull(),
            'author_id' => $this->integer(11)->notNull(),
        ]);
        $this->addPrimaryKey('', '{{%post_author_assn}}', ['post_id', 'author_id']);
    }

    public function safeDown()
    {
        echo "Removing tables.\n";
        $this->dropTable('{{%post_author_assn}}');
    }

}
